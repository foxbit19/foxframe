import { Component } from 'react';
import { FormComponentProps } from 'antd/lib/form';
export interface LoginFormProps {
    logoImage: any;
    requiredMessage?: string;
    onlyLogo?: boolean;
    onSubmit: () => any;
}
declare class LoginForm extends Component<LoginFormProps & FormComponentProps> {
    private form;
    private formLayout;
    constructor(props: LoginFormProps & FormComponentProps);
    render(): JSX.Element;
}
export declare const FoxLoginForm: import("antd/lib/form/interface").ConnectedComponentClass<typeof LoginForm, Pick<LoginFormProps & FormComponentProps<any>, "logoImage" | "requiredMessage" | "onlyLogo" | "onSubmit" | "wrappedComponentRef">>;
export {};
