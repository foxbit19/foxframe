import React from 'react';
export interface LogoProps {
    squareLogo?: any;
    landscapeLogo: any;
    collapsed?: boolean;
}
export declare const Logo: React.FC<LogoProps>;
