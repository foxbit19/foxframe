module.exports = function(grunt) {
    grunt.initConfig({
        copy: {
            less: {
                src: ['**/*.less'],
                dest: './lib',
                cwd: 'src',
                expand: true,
            },
            png: {
                src: ['**/*.png'],
                dest: './lib',
                cwd: 'src',
                expand: true,
            },
        },
    });

    require('grunt-contrib-copy')(grunt);

    grunt.registerTask('default', ['copy']);
};
