# FoxFrame

[![Coverage Status](https://coveralls.io/repos/github/foxbit19/foxframe/badge.svg?branch=master)](https://coveralls.io/github/foxbit19/foxframe?branch=master)
[![npm version](https://badge.fury.io/js/foxframe.svg)](https://badge.fury.io/js/foxframe)
[![Storybook Status](https://api.netlify.com/api/v1/badges/3694cfd9-f47c-4c41-8299-326e2d423df0/deploy-status)](https://app.netlify.com/sites/objective-kirch-180747/deploys)

A set of components on top of [ant-design](http://ant.design) to create react apps as fast as possible.

