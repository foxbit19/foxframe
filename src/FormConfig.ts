import {ReactNode, ReactElement} from 'react';

export default interface FormConfig {
    name: string;
    label: string;
    required?: boolean;
    initialValue?: any;
    component: ReactElement;
    tabName?: string;
    dependsOnFields?: string[];
    dependenceFormat?: (value: any, fieldName: string) => any;
}
