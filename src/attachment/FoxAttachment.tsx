import React from 'react';
import './attachment.less';
import {Typography, Button, Icon} from 'antd';
import DownloadLink from 'react-download-link';

const {Text} = Typography;

interface Props {
    id: number;
    name: string;
    readonly?: boolean;
    singleMode?: boolean;
    downloadAction: (attachmentId: number) => void;
    deleteAction: (attachmentId: number) => void;
}

const FoxAttachment: React.FC<Props> = props => {
    const {id, name, readonly, downloadAction, deleteAction} = props;

    return (
        <div className={props.singleMode ? 'paper-single' : 'paper'}>
            <Text className="name" strong>
                {name}
            </Text>
            <div className="actions">
                <DownloadLink
                    style={{margin: 0, marginLeft: 5}}
                    label={<Icon type="download" />}
                    filename={name}
                    exportFile={() => downloadAction(id)}
                />
                {readonly ? (
                    <div />
                ) : (
                    <Icon type="delete" className="download" onClick={() => deleteAction(id)} />
                )}
            </div>
        </div>
    );
};

export default FoxAttachment;
