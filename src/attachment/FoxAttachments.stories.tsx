import * as React from 'react';
import {storiesOf} from '@storybook/react';
import 'antd/dist/antd.less';
import FoxAttachments from './FoxAttachments';
import {action} from '@storybook/addon-actions';

storiesOf('Attachment', module)
    .add('simple', () => (
        <div style={{padding: '20px'}}>
            <FoxAttachments
                value={[
                    {
                        name: 'attachment 1',
                    },
                    {
                        name: 'attachment 2',
                    },
                    {
                        name: 'attachment 3',
                    },
                ]}
                onUpload={action('handle attachment upload')}
                onDownload={action('handle attachment download')}
                onDelete={action('handle attachment delete')}
            />
        </div>
    ))
    .add('readonly', () => (
        <div style={{padding: '20px'}}>
            <FoxAttachments
                value={[
                    {
                        name: 'attachment 1',
                    },
                    {
                        name: 'attachment 2',
                    },
                    {
                        name: 'attachment 3',
                    },
                ]}
                readonly
                onUpload={action('handle attachment upload')}
                onDownload={action('handle attachment download')}
                onDelete={action('handle attachment delete')}
            />
        </div>
    ))
    .add('single mode', () => (
        <div style={{padding: '20px'}}>
            <FoxAttachments
                value={[
                    {
                        name: 'attachment',
                    },
                ]}
                singleMode
                onUpload={action('handle attachment upload')}
                onDownload={action('handle attachment download')}
                onDelete={action('handle attachment delete')}
            />
        </div>
    ));
