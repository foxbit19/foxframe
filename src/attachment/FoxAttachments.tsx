import React, {useState, useCallback} from 'react';
import {Typography} from 'antd';
import './attachments.less';
import FoxAttachment from './FoxAttachment';
import Attachment from './Attachment';
import {useDropzone} from 'react-dropzone';

interface Props {
    value?: Attachment[];
    onChange?: (value: any) => void;
    readonly?: boolean;
    singleMode?: boolean;
    onUpload: (file: any[]) => void;
    onDownload: (attachmentId: number) => void;
    onDelete: (attachmentId: number) => void;
}

const FoxAttachments: React.FC<Props> = props => {
    const [attachmentList, setAttachmentList] = useState(props.value ? props.value : []);

    const onDrop = useCallback(acceptedFiles => {
        if (props.singleMode) {
            setAttachmentList([acceptedFiles[0]]);
            props.onUpload(acceptedFiles[0]);
        } else {
            setAttachmentList([...attachmentList, ...acceptedFiles]);
            props.onUpload(acceptedFiles);
        }
    }, []);
    const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop});

    return (
        <div>
            <div {...getRootProps()}>
                {props.readonly ? <input {...getInputProps()} /> : ''}
                <div className={!props.readonly && isDragActive ? 'content-drag' : 'content'}>
                    {attachmentList && attachmentList.length > 0 ? (
                        attachmentList.map((attachment, index) => (
                            <FoxAttachment
                                id={index}
                                name={attachment.name}
                                // extension="pdf"
                                downloadAction={(attachmentId: number) =>
                                    props.onDownload(attachmentId)
                                }
                                deleteAction={(attachmentId: number) =>
                                    props.onDelete(attachmentId)
                                }
                                readonly={props.readonly}
                                singleMode={props.singleMode}
                            />
                        ))
                    ) : (
                        <Typography>Nessun allegato</Typography>
                    )}
                </div>
            </div>
        </div>
    );
};

export default FoxAttachments;
