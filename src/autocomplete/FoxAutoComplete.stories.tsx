import * as React from 'react';
import {storiesOf} from '@storybook/react';
import {FoxAutoComplete} from './FoxAutoComplete';
import 'antd/dist/antd.less';
import {Button} from 'antd';
import {State, Store} from '@sambego/storybook-state';

const values = [
    {
        id: 0,
        name: 'element 1',
        description: 'description 1',
    },
    {
        id: 1,
        name: 'element 2',
        description: 'description 2',
    },
    {
        id: 2,
        name: 'element 3',
        description: 'description 3',
    },
    {
        id: 3,
        name: 'element 4',
        description: 'description 4',
    },
    {
        id: 4,
        name: 'element 5',
        description: 'description 5',
    },
];

const store = new Store({
    currentElement: '1',
});

const setNextElement = () => {
    if (store.get('currentElement')) {
        store.set({
            currentElement: ((+store.get('currentElement') + 1) % values.length).toString(),
        });
    } else {
        store.set({currentElement: '1'});
    }
};

storiesOf('Autocomplete', module)
    .add('simple', () => (
        <div style={{padding: '20px'}}>
            <div>type 'element' to use autocomplete</div>
            <FoxAutoComplete entities={values} textField="name" valueField="id" />
        </div>
    ))
    .add('simple with min chars', () => (
        <div style={{padding: '20px'}}>
            <div>type 'element' with at least 2 chars to use autocomplete</div>
            <FoxAutoComplete entities={values} textField="name" valueField="id" searchMinChar={2} />
        </div>
    ))
    .add('change value programmatically', () => (
        <div style={{padding: '20px'}}>
            <State store={store}>
                {state => [
                    <FoxAutoComplete
                        entities={values}
                        value={state.currentElement}
                        textField="name"
                        valueField="id"
                    />,
                ]}
            </State>
            <Button onClick={setNextElement}>Set to next element</Button>
        </div>
    ));
