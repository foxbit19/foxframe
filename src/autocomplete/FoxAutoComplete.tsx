import React, {useState, useEffect} from 'react';
import {AutoComplete} from 'antd';
import {DataSourceItemType} from 'antd/lib/auto-complete';
import {SelectValue} from 'antd/lib/select';

interface Props {
    placeholder?: string;
    entities: any[];
    valueField: string;
    textField: string | string[];
    value?: string;
    onChange?: (value: any) => void;
    searchMinChar?: number;
}

export interface InputState {
    value: any;
}

export const FoxAutoComplete: React.FC<Props> = (props: Props, state: InputState) => {
    const [dataSource, setDataSource] = useState<DataSourceItemType[]>([]);
    const [currentValue, setCurrentValue] = useState();

    useEffect(() => {
        if (props.value) {
            setCurrentValue(
                composeTextField(
                    props.textField,
                    props.entities.find((entity: any) =>
                        props.value ? entity.id === +props.value : false
                    )
                )
            );
            if (props.onChange) {
                props.onChange(props.value);
            }
        }
    }, [props.value]);

    const onSearch = (searchText: string) => {
        if (searchText.length > (props.searchMinChar ? props.searchMinChar : 1)) {
            setSearchable(
                !searchText
                    ? []
                    : props.entities
                    ? props.entities.filter((entity: any) =>
                          isSearchTextIncluded(entity, searchText)
                      )
                    : []
            );
        }

        setCurrentValue(searchText);
    };

    const onSelect = (value: SelectValue) => {
        if (props.onChange) {
            props.onChange(value);
        }
    };

    const isSearchTextIncluded = (entity: any, searchText: string) => {
        return (
            entity &&
            composeTextField(props.textField, entity)
                .toLowerCase()
                .includes(searchText.toLowerCase())
        );
    };

    const composeTextField = (textField: string | string[], entity: any): string => {
        let entityText: string = '';

        if (entity) {
            if (textField instanceof Array) {
                entityText = textField.map((field: string) => entity[field]).join(' ');
            } else {
                entityText = entity[textField];
            }
        }

        return entityText;
    };

    const setSearchable = (filteredEntities: any[]) => {
        if (filteredEntities.length > 0) {
            setDataSource(
                filteredEntities.map((filteredEntity: any) => ({
                    text: composeTextField(props.textField, filteredEntity),
                    value: filteredEntity[props.valueField].toString(),
                }))
            );
        }
    };

    const getDefaultValue = (): string | undefined => {
        if (state.value) {
            const idValue: number = +state.value;
            const textField: string = composeTextField(
                props.textField,
                props.entities.find((entity: any) => entity.id === idValue)
            );
            // onSearch(textField);
            return textField;
        } else {
            return;
        }
    };

    return (
        <AutoComplete
            dataSource={dataSource}
            onSearch={onSearch}
            placeholder={props.placeholder}
            onSelect={onSelect}
            defaultValue={getDefaultValue()}
            value={currentValue}
        />
    );
};
