import React, {Component} from 'react';
import Form, {FormComponentProps} from 'antd/lib/form';
import {WrappedFormUtils} from 'antd/lib/form/Form';
import FormConfig from '../FormConfig';

interface DynamicFormProps {
    config: FormConfig[];
    entity?: any;
    onFieldChange: (changedFieldName: string) => void;
}

class DynamicForm extends Component<DynamicFormProps & FormComponentProps> {
    private form: WrappedFormUtils;
    private formLayout: any;

    constructor(props: DynamicFormProps & FormComponentProps) {
        super(props);
        this.form = props.form;
        this.formLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 16},
            },
        };
    }

    componentDidMount() {
        if (this.props.entity) {
            for (const config of this.props.config) {
                this.props.onFieldChange(config.name);
            }
        }
    }

    public render() {
        return <Form {...this.formLayout}>{this.buildForm()}</Form>;
    }

    private buildForm() {
        return this.props.config.map((config: FormConfig, index: number) => {
            return (
                <Form.Item label={config.label} key={index}>
                    {this.form.getFieldDecorator(config.name, {
                        rules: [
                            {required: config.required, message: 'Questo campo è obbligatorio'},
                        ],
                        initialValue: this.props.entity
                            ? this.props.entity[config.name]
                            : config.initialValue
                            ? config.initialValue
                            : '',
                        // getValueFromEvent: (value: any) => this.handleDependsOn(value, config),
                    })(config.component)}
                </Form.Item>
            );
        });
    }
}

const handleDependsOn = (
    changedFieldName: any,
    props: DynamicFormProps & FormComponentProps,
    allValues: any
) => props.onFieldChange(changedFieldName);

export const FoxDynamicForm = Form.create<DynamicFormProps & FormComponentProps>({
    name: 'dynamic_form',
    onValuesChange: (
        props: DynamicFormProps & FormComponentProps,
        changedValues: any,
        allValues: any
    ) => handleDependsOn(changedValues, props, allValues),
})(DynamicForm);
