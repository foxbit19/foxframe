import React from 'react';
import {KanbanList} from './KanbanList';
import {DragDropContext, DropResult, ResponderProvided} from 'react-beautiful-dnd';
import {FoxActionType} from '../manageActions/FoxActionType';

interface Props {
    entities: any[];
    statusTypes: string[];
    statusLabels: string[];
    statusField: string;
    idField: string;
    titleField: string;
    descriptionField: string;
    infoField: string;
    onStatusChange: (entityId: string, status: string) => void;
    onAction?: (actionType: FoxActionType, entity: any) => void;
}

export const Kanban: React.FC<Props> = (props: Props) => {
    const findStatus = (entity: any) => {
        const fieldsChain: string[] = props.statusField.split('.');
        let status: any = entity;

        for (const field of fieldsChain) {
            status = status[field];
            if (status instanceof Array) {
                status = status[0];
            }
        }

        return status;
    };

    const getKanbanLists = () =>
        props.statusTypes.map((status: string, index: number) => (
            <KanbanList
                id={status}
                key={index}
                listTitle={props.statusLabels[index]}
                idField={props.idField}
                titleField={props.titleField}
                descriptionField={props.descriptionField}
                infoField={props.infoField}
                items={props.entities.filter((entity: any) => findStatus(entity) === status)}
                onAction={props.onAction}
            />
        ));

    const handleDragEnd = (result: DropResult, provided: ResponderProvided) => {
        if (!result.destination || result.destination.droppableId === result.source.droppableId) {
            return;
        }
        props.onStatusChange(result.draggableId, result.destination.droppableId);
    };

    return (
        <DragDropContext onDragEnd={handleDragEnd}>
            <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-around'}}>
                {getKanbanLists()}
            </div>
        </DragDropContext>
    );
};
