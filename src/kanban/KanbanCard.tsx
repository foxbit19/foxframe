import React from 'react';
import {Draggable, DraggableProvided, DraggableStateSnapshot} from 'react-beautiful-dnd';
import {List, Icon, Avatar} from 'antd';
import 'antd/dist/antd.less';
import './style.less';
import {FoxActionType} from '../manageActions/FoxActionType';

interface Props {
    id: number;
    title: string;
    description: string;
    info: string;
    onAction?: (type: FoxActionType) => void;
}

export const KanbanCard: React.FC<Props> = (props: Props) => {
    return (
        <Draggable key={props.id} draggableId={props.id.toString()} index={props.id}>
            {(dragProvided: DraggableProvided, dragSnapshot: DraggableStateSnapshot) => (
                <div
                    ref={dragProvided.innerRef}
                    {...dragProvided.draggableProps}
                    {...dragProvided.dragHandleProps}
                >
                    <List.Item
                        key={props.id}
                        className={dragSnapshot.isDragging ? 'dragging' : 'standard'}
                        actions={[
                            <Icon
                                type="eye"
                                onClick={() => props.onAction && props.onAction(FoxActionType.view)}
                            />,
                            <Icon
                                type="edit"
                                onClick={() =>
                                    props.onAction && props.onAction(FoxActionType.update)
                                }
                            />,
                            <Icon
                                type="delete"
                                onClick={() =>
                                    props.onAction && props.onAction(FoxActionType.delete)
                                }
                            />,
                        ]}
                    >
                        <List.Item.Meta
                            style={{cursor: 'hand'}}
                            description={props.description}
                            // onDoubleClick={handleItemSelect(item)}
                            avatar={
                                <Avatar>
                                    <Icon type="user" />
                                </Avatar>
                            }
                            title={props.title}
                        />
                    </List.Item>
                </div>
            )}
        </Draggable>
    );
};
