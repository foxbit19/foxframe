import React, {CSSProperties} from 'react';
import {Droppable, DroppableProvided, DroppableStateSnapshot} from 'react-beautiful-dnd';
import {List} from 'antd';
import {KanbanCard} from './KanbanCard';
import {FoxActionType} from '../manageActions/FoxActionType';
import Utility from '../utility';

const overList: CSSProperties = {
    background: '#bfbfbf',
    margin: '10px',
};

const list: CSSProperties = {
    background: '#efefef',
    margin: '10px',
};

interface Props {
    id: string;
    listTitle: string;
    items: any[];
    idField: string;
    titleField: string;
    descriptionField: string;
    infoField: string;
    onAction?: (actionType: FoxActionType, entity: any) => void;
}

export const KanbanList: React.FC<Props> = (props: Props) => {
    const handleAction = (actionType: FoxActionType, entityId: number) =>
        props.onAction && props.onAction(actionType, entityId);

    return (
        <Droppable droppableId={props.id}>
            {(provided: DroppableProvided, snapshot: DroppableStateSnapshot) => (
                <div ref={provided.innerRef} style={snapshot.isDraggingOver ? overList : list}>
                    <List
                        bordered
                        style={{minWidth: '300px', minHeight: '400px'}}
                        header={<div>{props.listTitle}</div>}
                    >
                        {props.items.map((item: any, index: number) => (
                            <KanbanCard
                                key={index}
                                id={Utility.extractField(item, props.idField)}
                                title={Utility.extractField(item, props.titleField)}
                                info={Utility.extractField(item, props.infoField)}
                                description={Utility.extractField(item, props.descriptionField)}
                                onAction={(actionType: FoxActionType) =>
                                    handleAction(actionType, item)
                                }
                            />
                        ))}
                    </List>
                    {provided.placeholder}
                </div>
            )}
        </Droppable>
    );
};
