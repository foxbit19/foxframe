import React from 'react';

interface Props {
    value?: any;
    fieldNames?: string[];
}

export const FoxLabel: React.FC<Props> = (props: Props) => {
    const concatFields = () =>
        props.fieldNames ? props.fieldNames.map((fieldName: string) => props.value[fieldName]).join(' ') : props.value;

    return <div>{props.value ? concatFields() : '-'}</div>;
};
