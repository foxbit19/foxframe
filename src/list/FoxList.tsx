import React from 'react';
import {List, Avatar, Button, Divider} from 'antd';
import {FoxLabel} from '../label/FoxLabel';
import FoxAction from '../manageActions/FoxAction';
import {FoxActionType} from '../manageActions/FoxActionType';

interface Props {
    value?: any[];
    titleField: string;
    descriptionFields: string[];
    typeField: string;
    typeTextField: {
        [fieldName: string]: string;
    };
    actions?: FoxAction[];
    onAction?: (selectedActionType: FoxActionType, selectedEntity: any) => void;
}

export const FoxList: React.FC<Props> = (props: Props) => {
    const getCurrentValue = (): any[] => {
        if (props.value) {
            return props.value;
        } else {
            return [];
        }
    };

    const handleAction = (selectedAction: FoxAction, selectedEntity: any) => {
        if (selectedAction.type === FoxActionType.custom && selectedAction.action) {
            selectedAction.action(selectedEntity);
        } else {
            if (props.onAction) {
                props.onAction(selectedAction.type, selectedEntity);
            }
        }
    };

    return (
        <List
            itemLayout="horizontal"
            dataSource={getCurrentValue()}
            renderItem={item => (
                <List.Item
                    actions={[
                        !props.actions
                            ? ''
                            : props.actions.map((action: FoxAction, index: number) => (
                                  <span key={index}>
                                      <Button
                                          type="link"
                                          size="small"
                                          onClick={() => handleAction(action, item)}
                                      >
                                          {action.name}
                                      </Button>
                                      <Divider type="vertical" />
                                  </span>
                              )),
                    ]}
                >
                    <List.Item.Meta
                        avatar={<Avatar>{props.typeTextField[item[props.typeField]]}</Avatar>}
                        title={item[props.titleField]}
                        description={<FoxLabel value={item} fieldNames={props.descriptionFields} />}
                    />
                </List.Item>
            )}
        />
    );
};
