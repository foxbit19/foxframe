import * as React from 'react';
import {storiesOf} from '@storybook/react';
import {FoxLoginForm} from './FoxLoginForm';
import {action} from '@storybook/addon-actions';

storiesOf('Login', module).add('simple', () => (
    <FoxLoginForm logoImage="./logo-landscape.png" onSubmit={action('form submit')} />
));
