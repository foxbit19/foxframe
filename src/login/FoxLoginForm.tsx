import React, {Component, FormEvent} from 'react';
import {Form, Input, Icon, Button, Layout} from 'antd';
import {FormComponentProps} from 'antd/lib/form';
import {Logo} from '../logo/Logo';
import * as CSS from 'csstype';
import {WrappedFormUtils} from 'antd/lib/form/Form';

const loginFormStyles: CSS.Properties = {
    background: '#fff',
    width: '300px',
    height: '300px',
    padding: '20px',
    border: '1px solid #a3a3a3',
    borderRadius: '4px',
    position: 'absolute',
    top: '0',
    bottom: '0',
    left: '0',
    right: '0',
    margin: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
};

export interface LoginFormProps {
    logoImage: any;
    requiredMessage?: string;
    onlyLogo?: boolean;
    onSubmit: () => any;
    className?: string;
}

class LoginForm extends Component<LoginFormProps & FormComponentProps> {
    private form: WrappedFormUtils;
    private formLayout: any;

    constructor(props: LoginFormProps & FormComponentProps) {
        super(props);
        this.form = props.form;
    }

    render() {
        const {getFieldDecorator} = this.props.form;

        return (
            <div className={this.props.className}>
                <Form style={loginFormStyles}>
                    <Logo landscapeLogo={this.props.logoImage} />
                    <Form.Item>
                        {getFieldDecorator('username', {
                            rules: [
                                {
                                    required: true,
                                    message: this.props.requiredMessage
                                        ? this.props.requiredMessage
                                        : 'Questo campo è obbligatorio!',
                                },
                            ],
                        })(
                            <Input
                                prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}} />}
                                placeholder="Username"
                            />
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('password', {
                            rules: [
                                {
                                    required: true,
                                    message: this.props.requiredMessage
                                        ? this.props.requiredMessage
                                        : 'Questo campo è obbligatorio!',
                                },
                            ],
                        })(
                            <Input
                                prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}} />}
                                type="password"
                                placeholder="Password"
                            />
                        )}
                    </Form.Item>
                    <Button
                        type="primary"
                        htmlType="submit"
                        onClick={this.props.onSubmit}
                        className="login-form-button"
                    >
                        Log in
                    </Button>
                </Form>
            </div>
        );
    }
}

export const FoxLoginForm = Form.create<LoginFormProps & FormComponentProps>({name: 'login'})(
    LoginForm
);
