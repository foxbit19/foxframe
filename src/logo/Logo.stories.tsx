import * as React from 'react';
import {storiesOf} from '@storybook/react';
import {Logo} from './Logo';
import {Layout, Menu} from 'antd';
import {Store} from '@sambego/storybook-state';
import 'antd/dist/antd.less';

const {Header, Sider} = Layout;

const store = new Store({
    collapsed: true,
});

const toggleCollapsed = () => {
    if (store.get('collapsed')) {
        store.set({
            collapsed: !store.get('collapsed'),
        });
    } else {
        store.set({collapsed: true});
    }
};

storiesOf('Logo', module)
    .add('simple', () => <Logo squareLogo="./logo.png" landscapeLogo="./logo-landscape.png" />)
    .add('collapsed', () => (
        <Logo squareLogo="./logo.png" landscapeLogo="./logo-landscape.png" collapsed />
    ));
/* .add('inside navigation', () => (
        <Layout>
            <Layout>
                <Sider
                    width={230}
                    collapsible
                    collapsed={store.get('collapsed')}
                    onCollapse={toggleCollapsed}
                    theme="light"
                >
                    <Logo squareLogo="./logo.png" landscapeLogo="./logo-landscape.png" collapsed />
                    <Menu mode="inline" defaultSelectedKeys={['1']} theme="light">
                        <Menu.Item key="1">Navigation 1</Menu.Item>
                        <Menu.Item key="2">Navigation 2</Menu.Item>
                        <Menu.Item key="3">Navigation 3</Menu.Item>
                        <Menu.Item key="4">Navigation 4</Menu.Item>
                    </Menu>
                </Sider>
            </Layout>
            <Layout>
                <Header className="header" />
            </Layout>
        </Layout>
    )); */
