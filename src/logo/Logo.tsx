import React from 'react';
import './style.less';

export interface LogoProps {
    squareLogo?: any;
    landscapeLogo: any;
    collapsed?: boolean;
}

export const Logo: React.FC<LogoProps> = (props: LogoProps) => {
    return (
        <div className="logo">
            <img
                alt="logo"
                className="img"
                src={props.collapsed && props.squareLogo ? props.squareLogo : props.landscapeLogo}
            />
        </div>
    );
};
