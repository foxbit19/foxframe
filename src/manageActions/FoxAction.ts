import {FoxActionType} from './FoxActionType';

export default interface FoxAction {
    name: string;
    type: FoxActionType;
    /**
     * Function used by this action.
     * This field is used only when type field is set to 'custom'
     */
    action?: (entity: any) => void;
    groupName?: string;
    icon: string;
    disabledField?: string; // this field is used by disabledCheck
    disabledCheck?: any; // if disabledField of entity is equal to this field, this action is disabled
}
