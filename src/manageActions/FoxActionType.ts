export enum FoxActionType {
    view,
    create,
    update,
    delete,
    todo,
    doing,
    done,
    custom,
}
