import React, {useEffect, useState} from 'react';
import {Modal} from 'antd';
import {FoxDynamicForm} from '../dynamicForm/DynamicForm';
import FormConfig from '../FormConfig';
import {FoxActionType} from './FoxActionType';
import {FoxTabs} from '../tabs/FoxTabs';

const {confirm} = Modal;

interface Props {
    formConfig: FormConfig[];
    viewConfig?: FormConfig[];
    entity: any;
    triggerAction: FoxActionType | null;
    onCreate: (entity: any) => void;
    onUpdate: (entity: any) => void;
    onDelete: (entity: any) => void;
    onActionEnd: () => void;
    modalFullWidth?: boolean;
    forceRenderTabs?: boolean;
}

export const FoxManageActions: React.FC<Props> = (props: Props) => {
    const formRefs: any[] = [];
    const [confirmLoading, setConfirmLoading] = useState(false);

    const setRef = (ref: any) => {
        if (ref) {
            formRefs.push(ref);
        }

        if (formRefs.length > 0) {
            formRefs.forEach(formRef => formRef.form.resetFields());
        }
    };
    const hideModal = () => props.onActionEnd();
    const validate = () => {
        let entity: any;
        let hasError: boolean = false;

        for (const formRef of formRefs) {
            formRef.form.validateFields((err: any, values: any) => {
                if (!err) {
                    entity = entity ? Object.assign(entity, values) : values;
                    hideModal();
                } else {
                    hasError = true;
                }
            });

            if (hasError) {
                break;
            }
        }

        if (!hasError) {
            return entity;
        }
    };
    const handleCreate = async () => {
        const entity: any = await validate();
        if (entity) {
            setConfirmLoading(true);
            props.onCreate(entity);
            setConfirmLoading(false);
        }
    };
    const handleUpdate = async () => {
        const entity: any = await validate();
        if (entity) {
            setConfirmLoading(true);
            entity.id = props.entity.id;
            props.onUpdate(entity);
            setConfirmLoading(false);
        }
    };

    /**
     * This function updates the value of all dependencies field of the changed one
     * @param changedFieldName The name of the changed field
     * @param changeFieldValue The value of the changed field
     */
    const handleUpdateDependenciesFields = (changedFieldName: string) => {
        for (const config of props.formConfig) {
            // this loop finds dependencies fields
            if (
                config.dependsOnFields &&
                config.dependenceFormat &&
                config.dependsOnFields.includes(Object.keys(changedFieldName)[0])
            ) {
                const format = config.dependenceFormat(
                    Object.values(changedFieldName)[0],
                    Object.keys(changedFieldName)[0]
                );
                if (format) {
                    // there are many forms to loop through
                    for (const formRef of formRefs) {
                        formRef.form.setFieldsValue({
                            [config.name]: format,
                        });
                    }
                }
            }
        }
    };

    useEffect(() => {
        const showDeleteConfirm = () => {
            confirm({
                title: `Desideri cancellare questo record?`,
                content: "La cancellazione di questo record è un'operazione irreversibile",
                onOk() {
                    props.onDelete(props.entity);
                },
                onCancel() {
                    props.onActionEnd();
                },
            });
        };
        if (props.triggerAction === FoxActionType.delete) {
            showDeleteConfirm();
        }
        // eslint-disable-next-line
    }, [props.triggerAction]);

    return (
        <div>
            <Modal
                visible={props.triggerAction === FoxActionType.view}
                title="Visualizza"
                onCancel={hideModal}
                onOk={hideModal}
                width={
                    props.modalFullWidth ? window.innerWidth - 0.1 * window.innerWidth : undefined
                }
            >
                <FoxTabs
                    config={props.viewConfig ? props.viewConfig : []}
                    entity={props.entity}
                    forceRender={props.forceRenderTabs}
                />
            </Modal>
            <Modal
                visible={props.triggerAction === FoxActionType.create}
                title="Nuovo"
                onCancel={hideModal}
                onOk={handleCreate}
                confirmLoading={confirmLoading}
                width={
                    props.modalFullWidth ? window.innerWidth - 0.1 * window.innerWidth : undefined
                }
            >
                <FoxTabs
                    config={props.formConfig}
                    wrappedComponentRef={setRef}
                    onFieldChange={handleUpdateDependenciesFields}
                    forceRender={props.forceRenderTabs}
                />
            </Modal>
            <Modal
                visible={props.triggerAction === FoxActionType.update}
                title="Modifica"
                onCancel={hideModal}
                onOk={handleUpdate}
                confirmLoading={confirmLoading}
                width={
                    props.modalFullWidth ? window.innerWidth - 0.1 * window.innerWidth : undefined
                }
            >
                <FoxTabs
                    config={props.formConfig}
                    wrappedComponentRef={setRef}
                    entity={props.entity}
                    onFieldChange={handleUpdateDependenciesFields}
                    forceRender={props.forceRenderTabs}
                />
            </Modal>
        </div>
    );
};
