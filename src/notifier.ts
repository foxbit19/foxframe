import {notification} from 'antd';

export enum NotificationType {
    info = 'info',
    success = 'success',
    error = 'error',
    warning = 'warning',
}

export default class Notifier {
    static notify(type: NotificationType, title: string, description: string) {
        return notification[type]({
            message: title,
            description: description,
            placement: 'bottomRight',
        });
    }
}
