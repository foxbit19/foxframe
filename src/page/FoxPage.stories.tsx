import * as React from 'react';
import {storiesOf} from '@storybook/react';
import {FoxPage} from './FoxPage';
import {FoxAutoComplete} from '../autocomplete/FoxAutoComplete';
import 'antd/dist/antd.less';
import {FoxActionType} from '../manageActions/FoxActionType';
import {Input, Icon} from 'antd';
import moment from 'moment';

let firstName: string = '';
let lastName: string = '';

const autocompleteValues = [
    {
        value: 1,
        text: 'autocomplete 1',
    },
    {
        value: 2,
        text: 'autocomplete 2',
    },
    {
        value: 3,
        text: 'autocomplete 3',
    },
    {
        value: 4,
        text: 'autocomplete 4',
    },
    {
        value: 5,
        text: 'autocomplete 5',
    },
];

const values = [
    {
        id: 1,
        name: 'element 1',
        description: 'description 1',
        status: '1',
        valid: true,
        insertedDate: new Date(2019, 12, 25, 15, 0),
    },
    {
        id: 2,
        name: 'element 2',
        description: 'description 2',
        status: '2',
        valid: true,
        insertedDate: new Date(2020, 1, 4, 9, 0),
    },
    {
        id: 3,
        name: 'element 3',
        description: 'description 3',
        status: '1',
        valid: false,
        insertedDate: new Date(2019, 12, 25, 15, 0),
    },
    {
        id: 4,
        name: 'element 4',
        description: 'description 4',
        status: '3',
        valid: false,
        insertedDate: new Date(2019, 10, 20, 17, 0),
    },
    {
        id: 5,
        name: 'element 5',
        description: 'description 5',
        status: '1',
        valid: true,
        insertedDate: new Date(2019, 11, 13, 14, 0),
    },
];

const subValues = [
    {
        id: 1,
        name: 'subelement 1',
        description: 'description 1',
        status: '1',
        parentId: 1,
    },
    {
        id: 2,
        name: 'subelement 2',
        description: 'description 2',
        status: '2',
        parentId: 1,
    },
    {
        id: 3,
        name: 'subelement 3',
        description: 'description 3',
        status: '1',
        parentId: 2,
    },
    {
        id: 4,
        name: 'subelement 4',
        description: 'description 4',
        status: '3',
        parentId: 3,
    },
    {
        id: 5,
        name: 'subelement 5',
        description: 'description 5',
        status: '1',
        parentId: 3,
    },
];

storiesOf('Page', module)
    .add('simple', () => (
        <FoxPage
            title="Fox page"
            entities={values}
            storeName="test"
            dispatch={() => ''}
            headerIcon={<Icon type="apple" />}
            tableColumns={[
                {
                    title: 'Name',
                    dataIndex: 'name',
                    key: 'name',
                },
                {
                    title: 'Description',
                    dataIndex: 'description',
                    key: 'description',
                },
                {
                    title: 'Valid',
                    dataIndex: 'valid',
                    key: 'valid',
                    render: (valid: boolean) =>
                        valid ? <Icon type="check-circle" /> : <Icon type="close-circle" />,
                },
                {
                    title: 'Inserted at',
                    dataIndex: 'insertedDate',
                    key: 'insertedDate',
                    render: (date: Date) => moment(date).format('DD/MM/YYYY HH:mm'),
                },
            ]}
            formConfig={[
                {
                    name: 'name',
                    label: 'Name',
                    required: true,
                    component: <Input />,
                },
                {
                    name: 'description',
                    label: 'Description',
                    component: <Input />,
                },
                {
                    name: 'otherField',
                    label: 'Other Field',
                    component: (
                        <FoxAutoComplete
                            entities={autocompleteValues}
                            textField="text"
                            valueField="value"
                        />
                    ),
                },
            ]}
            actions={[
                {
                    name: 'Edit',
                    type: FoxActionType.update,
                    icon: 'edit',
                },
                {
                    name: 'Delete',
                    type: FoxActionType.delete,
                    icon: 'delete',
                },
            ]}
            onAction={() => ''}
        />
    ))
    .add('without new button', () => (
        <FoxPage
            title="Fox page"
            hideNewButton
            entities={values}
            storeName="test"
            dispatch={() => ''}
            headerIcon="android"
            tableColumns={[
                {
                    title: 'Name',
                    dataIndex: 'name',
                    key: 'name',
                },
                {
                    title: 'Description',
                    dataIndex: 'description',
                    key: 'description',
                },
            ]}
            formConfig={[
                {
                    name: 'name',
                    label: 'Name',
                    required: true,
                    component: <Input />,
                },
                {
                    name: 'description',
                    label: 'Description',
                    component: <Input />,
                },
            ]}
            actions={[
                {
                    name: 'Edit',
                    type: FoxActionType.update,
                    icon: 'edit',
                },
                {
                    name: 'Delete',
                    type: FoxActionType.delete,
                    icon: 'delete',
                },
            ]}
            onAction={() => ''}
        />
    ))
    .add('fullwidth modals', () => (
        <FoxPage
            title="Fox page"
            entities={values}
            storeName="test"
            dispatch={() => ''}
            headerIcon="android"
            tableColumns={[
                {
                    title: 'Name',
                    dataIndex: 'name',
                    key: 'name',
                },
                {
                    title: 'Description',
                    dataIndex: 'description',
                    key: 'description',
                },
            ]}
            formConfig={[
                {
                    name: 'name',
                    label: 'Name',
                    required: true,
                    component: <Input />,
                },
                {
                    name: 'description',
                    label: 'Description',
                    component: <Input />,
                },
            ]}
            actions={[
                {
                    name: 'Edit',
                    type: FoxActionType.update,
                    icon: 'edit',
                },
                {
                    name: 'Delete',
                    type: FoxActionType.delete,
                    icon: 'delete',
                },
            ]}
            onAction={() => ''}
            modalFullWidth={true}
        />
    ))
    .add('action modals with tabs', () => (
        <FoxPage
            title="Fox page"
            entities={values}
            storeName="test"
            dispatch={() => ''}
            headerIcon="android"
            forceRenderTabs
            tableColumns={[
                {
                    title: 'Name',
                    dataIndex: 'name',
                    key: 'name',
                },
                {
                    title: 'Description',
                    dataIndex: 'description',
                    key: 'description',
                },
            ]}
            formConfig={[
                {
                    name: 'user.firstName',
                    label: 'First name',
                    required: true,
                    component: <Input />,
                    tabName: 'Required',
                },
                {
                    name: 'user.lastName',
                    label: 'Last name',
                    required: true,
                    component: <Input />,
                    tabName: 'Required',
                },
                {
                    name: 'description',
                    label: 'Description',
                    component: <Input />,
                    tabName: 'Optional',
                },
                {
                    name: 'fullname',
                    label: 'Full name',
                    component: <Input />,
                    tabName: 'Optional',
                    dependsOnFields: ['user'],
                    dependenceFormat: (value: string, fieldName: string) => {
                        console.log('handle field dependence ' + fieldName);

                        if (Object.keys(value)[0] === 'firstName') {
                            firstName = Object.values(value)[0];
                        } else if (Object.keys(value)[0] === 'lastName') {
                            lastName = Object.values(value)[0];
                        }

                        return `${firstName} ${lastName}`;
                    },
                },
                {
                    name: 'fullname_copy',
                    label: 'Full name copy ',
                    component: <Input />,
                    tabName: 'Optional',
                    dependsOnFields: ['fullname'],
                    dependenceFormat: (value: string, fieldName: string) => value,
                },
            ]}
            actions={[
                {
                    name: 'Edit',
                    type: FoxActionType.update,
                    icon: 'edit',
                },
                {
                    name: 'Delete',
                    type: FoxActionType.delete,
                    icon: 'delete',
                },
            ]}
            onAction={() => ''}
        />
    ))
    .add('with kanban', () => (
        <FoxPage
            title="Fox page"
            subtitle="with kanban"
            storeName="test"
            enableKanban={true}
            statusField="status"
            statusTypes={['1', '2', '3']}
            statusLabels={['todo', 'doing', 'done']}
            entities={values}
            dispatch={() => ''}
            headerIcon="android"
            tableColumns={[
                {
                    title: 'Name',
                    dataIndex: 'name',
                    key: 'name',
                },
                {
                    title: 'Description',
                    dataIndex: 'description',
                    key: 'description',
                },
            ]}
            formConfig={[
                {
                    name: 'name',
                    label: 'The name',
                    required: true,
                    component: <Input />,
                },
                {
                    name: 'description',
                    label: 'Description',
                    component: <Input />,
                },
            ]}
            actions={[
                {
                    name: 'Edit',
                    type: FoxActionType.create,
                    icon: 'edit',
                },
                {
                    name: 'Delete',
                    type: FoxActionType.delete,
                    icon: 'delete',
                },
            ]}
            onAction={() => ''}
        />
    ))
    .add('with expandable rows', () => (
        <FoxPage
            title="Fox page"
            subtitle="expandable rows"
            storeName="test"
            entities={values}
            dispatch={() => ''}
            headerIcon="android"
            expandableRows
            expandedEntities={expandedRow =>
                subValues.filter(value => value.parentId === expandedRow.id)
            }
            tableColumns={[
                {
                    title: 'Name',
                    dataIndex: 'name',
                    key: 'name',
                },
                {
                    title: 'Description',
                    dataIndex: 'description',
                    key: 'description',
                },
            ]}
            formConfig={[
                {
                    name: 'name',
                    label: 'Name',
                    required: true,
                    component: <Input />,
                },
                {
                    name: 'description',
                    label: 'Description',
                    component: <Input />,
                },
            ]}
            actions={[
                {
                    name: 'Edit',
                    type: FoxActionType.create,
                    icon: 'edit',
                },
                {
                    name: 'Delete',
                    type: FoxActionType.delete,
                    icon: 'delete',
                },
            ]}
            onAction={() => ''}
        />
    ));
