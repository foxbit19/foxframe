import React, {useState, ReactNode} from 'react';
import {Button, PageHeader, Switch, Icon} from 'antd';
import {FoxTable} from '../table/FoxTable';
import {ColumnProps} from 'antd/lib/table';
import FormConfig from '../FormConfig';
import {FoxManageActions} from '../manageActions/FoxManageAction';
import FoxAction from '../manageActions/FoxAction';
import {FoxActionType} from '../manageActions/FoxActionType';
import {Kanban} from '../kanban/Kanban';
import Utility from '../utility';

interface FoxPageProps {
    title: string;
    subtitle?: string;
    dispatch: any;
    entities: any[];
    storeName: string;
    tableColumns: ColumnProps<any>[];
    actions?: FoxAction[];
    headerIcon: ReactNode;
    hideNewButton?: boolean;
    extraButtons?: ReactNode[];
    formConfig?: FormConfig[];
    viewConfig?: FormConfig[];
    onAction?: (actionType: FoxActionType, entity: any) => void;
    enableKanban?: boolean;
    statusField?: string;
    statusTypes?: string[];
    statusLabels?: string[];
    onStatusChange?: (entityId: string, status: string) => void;
    kanbanIdField?: string;
    kanbanTitleField?: string;
    kanbanDescriptionField?: string;
    kanbanInfoField?: string;
    modalFullWidth?: boolean;
    /**
     * render all tabs using eager loading
     */
    forceRenderTabs?: boolean;
    expandableRows?: boolean;
    expandedEntities?: (expandedRow: any) => any;
}

export const FoxPage: React.FC<FoxPageProps> = props => {
    const [kanban, setKanban] = useState(false);
    const [entity, setEntity] = useState(null);
    const [triggerAction, setTriggerAction] = useState<FoxActionType | null>(FoxActionType.custom);

    const handleCreate = (entity: any) => props.dispatch(`${props.storeName}/create`, entity);
    const handleUpdate = (entity: any) => props.dispatch(`${props.storeName}/update`, entity);
    const handleDelete = (entity: any) => props.dispatch(`${props.storeName}/delete`, entity);
    const handleCreateActionCall = () => setTriggerAction(FoxActionType.create);
    const handleActionCall = (actionType: FoxActionType, selectedEntity: any) => {
        if (props.actions) {
            const actionToDisable: FoxAction | undefined = props.actions.find(
                (action: FoxAction) => action.type === actionType
            );
            if (
                actionToDisable &&
                actionToDisable.disabledField &&
                Utility.extractField(selectedEntity, actionToDisable.disabledField) ===
                    actionToDisable.disabledCheck
            ) {
                return;
            }
        }

        if (actionType === FoxActionType.custom && props.onAction) {
            props.onAction(actionType, selectedEntity);
        } else {
            setEntity(selectedEntity);
            setTriggerAction(actionType);
            props.onAction ? props.onAction(actionType, selectedEntity) : null;
        }
    };
    const getKanbanSwitch = () =>
        props.enableKanban ? (
            <Switch
                checkedChildren={<Icon type="project" />}
                unCheckedChildren={<Icon type="table" />}
                onChange={() => setKanban(!kanban)}
                key="1"
            />
        ) : (
            ''
        );

    const getNewButton = () =>
        props.hideNewButton ? (
            ''
        ) : (
            <Button
                hidden={props.hideNewButton}
                key="2"
                type="primary"
                onClick={handleCreateActionCall}
            >
                Nuovo
            </Button>
        );

    return (
        <div style={{background: '#f6f6f6'}}>
            <PageHeader
                ghost={false}
                avatar={{icon: props.headerIcon}}
                onBack={() => window.history.back()}
                title={props.title}
                subTitle={props.subtitle}
                extra={[getKanbanSwitch(), getNewButton(), props.extraButtons]}
            ></PageHeader>
            {props.enableKanban && kanban ? (
                <Kanban
                    entities={props.entities}
                    statusField={props.statusField ? props.statusField : ''}
                    statusTypes={props.statusTypes ? props.statusTypes : []}
                    statusLabels={props.statusLabels ? props.statusLabels : []}
                    idField={props.kanbanIdField ? props.kanbanIdField : ''}
                    titleField={props.kanbanTitleField ? props.kanbanTitleField : ''}
                    descriptionField={
                        props.kanbanDescriptionField ? props.kanbanDescriptionField : ''
                    }
                    infoField={props.kanbanInfoField ? props.kanbanInfoField : ''}
                    onStatusChange={props.onStatusChange ? props.onStatusChange : () => ''}
                    onAction={handleActionCall}
                />
            ) : props.expandableRows ? (
                <FoxTable
                    tableColumns={props.tableColumns}
                    tableData={props.entities}
                    actions={props.actions}
                    onAction={handleActionCall}
                    expandedRowRender={record => {
                        return (
                            <FoxTable
                                tableColumns={props.tableColumns}
                                tableData={
                                    props.expandedEntities ? props.expandedEntities(record) : []
                                }
                                actions={props.actions}
                                onAction={handleActionCall}
                                showHeader={false}
                            />
                        );
                    }}
                />
            ) : (
                <FoxTable
                    tableColumns={props.tableColumns}
                    tableData={props.entities}
                    actions={props.actions}
                    onAction={handleActionCall}
                />
            )}
            <FoxManageActions
                formConfig={props.formConfig ? props.formConfig : []}
                viewConfig={props.viewConfig}
                entity={entity}
                triggerAction={triggerAction}
                onCreate={handleCreate}
                onUpdate={handleUpdate}
                onDelete={handleDelete}
                onActionEnd={() => setTriggerAction(null)}
                modalFullWidth={props.modalFullWidth}
                forceRenderTabs={props.forceRenderTabs}
            />
        </div>
    );
};
