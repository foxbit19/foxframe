import * as React from 'react';
import {storiesOf} from '@storybook/react';
import {FoxSelect} from './FoxSelect';
import 'antd/dist/antd.less';
import {action} from '@storybook/addon-actions';

const values = [
    {
        id: 0,
        name: 'element 1',
        description: 'description 1',
    },
    {
        id: 1,
        name: 'element 2',
        description: 'description 2',
    },
    {
        id: 2,
        name: 'element 3',
        description: 'description 3',
    },
    {
        id: 3,
        name: 'element 4',
        description: 'description 4',
    },
    {
        id: 4,
        name: 'element 5',
        description: 'description 5',
    },
];

storiesOf('Select', module)
    .add('simple', () => (
        <div style={{width: '400px', padding: '20px'}}>
            <div>simple select</div>
            <FoxSelect entities={values} textField="name" valueField="id" />
        </div>
    ))
    .add('first option default selected', () => (
        <div style={{padding: '20px'}}>
            <div>first option default selected</div>
            <FoxSelect
                entities={values}
                textField="name"
                valueField="id"
                defaultSelectedFirstOption
                onChange={action('changed value')}
            />
        </div>
    ))
    .add('custom selected option', () => (
        <div style={{padding: '20px'}}>
            <div>custom selected option</div>
            <FoxSelect entities={values} textField="name" valueField="id" value={3} />
        </div>
    ));
