import React, {useState, useEffect} from 'react';
import {Select} from 'antd';

const {Option} = Select;

interface Props {
    entities: any[];
    textField: string;
    valueField: string;
    onChange?: (value: any) => void;
    defaultSelectedFirstOption?: boolean;
    value?: any;
}

export const FoxSelect: React.FC<Props> = (props: Props) => {
    const [defaultValue, setDefaultValue] = useState();

    useEffect(() => {
        const triggerOnChange = (value: any) => {
            if (props.onChange) {
                props.onChange(value);
            }
        };

        const handleDefaultValue = () => {
            if (props.value) {
                setDefaultValue(props.value);
                triggerOnChange(props.value);
            } else {
                if (props.defaultSelectedFirstOption) {
                    setDefaultValue(props.entities[0][props.valueField]);
                    triggerOnChange(props.entities[0][props.valueField]);
                }
            }
        };

        handleDefaultValue();
    }, []);

    return (
        <Select onChange={props.onChange} value={defaultValue}>
            {props.entities.map((element: any, index: number) => {
                const value: string = props.valueField ? element[props.valueField] : element;
                const text: string = props.textField ? element[props.textField] : element;
                return (
                    <Option key={index} value={value}>
                        {text}
                    </Option>
                );
            })}
        </Select>
    );
};
