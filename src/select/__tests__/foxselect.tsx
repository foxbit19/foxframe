import {shallow, ShallowWrapper, mount} from 'enzyme';
import sinon from 'sinon';
import {FoxSelect} from '../FoxSelect';
import React from 'react';
import {Select} from 'antd';

const {Option} = Select;

const entities = [
    {
        id: 0,
        name: 'element 1',
        description: 'description 1',
    },
    {
        id: 1,
        name: 'element 2',
        description: 'description 2',
    },
    {
        id: 2,
        name: 'element 3',
        description: 'description 3',
    },
    {
        id: 3,
        name: 'element 4',
        description: 'description 4',
    },
    {
        id: 4,
        name: 'element 5',
        description: 'description 5',
    },
];

describe('<FoxSelect />', () => {
    it('renders correctly', () => {
        const wrapper = shallow(<FoxSelect entities={entities} textField="name" valueField="id" />);
        expect(wrapper).toMatchSnapshot();
    });

    it('render the correct number of elements', () => {
        const wrapper = shallow(<FoxSelect entities={entities} textField="name" valueField="id" />);
        expect(wrapper.find(Option)).toHaveLength(5);
    });

    it('render elements value correctly', () => {
        const wrapper = shallow(<FoxSelect entities={entities} textField="name" valueField="id" />);

        expect(wrapper.find(Option).get(0).props.value).toBe(0);
        expect(wrapper.find(Option).get(1).props.value).toBe(1);
        expect(wrapper.find(Option).get(2).props.value).toBe(2);
        expect(wrapper.find(Option).get(3).props.value).toBe(3);
        expect(wrapper.find(Option).get(4).props.value).toBe(4);
    });

    it('render elements name', () => {
        const wrapper = shallow(<FoxSelect entities={entities} textField="name" valueField="id" />);

        expect(wrapper.find(Option).get(0).props.children).toBe('element 1');
        expect(wrapper.find(Option).get(1).props.children).toBe('element 2');
        expect(wrapper.find(Option).get(2).props.children).toBe('element 3');
        expect(wrapper.find(Option).get(3).props.children).toBe('element 4');
        expect(wrapper.find(Option).get(4).props.children).toBe('element 5');
    });

    it('render first element by default', () => {
        let changedValue;
        mount(
            <FoxSelect
                entities={entities}
                textField="name"
                valueField="id"
                defaultSelectedFirstOption
                onChange={(value: any) => (changedValue = value)}
            />
        );

        expect(changedValue).toBe(0);
    });

    it('render custom element by default', () => {
        const wrapper = mount(
            <FoxSelect
                entities={entities}
                textField="name"
                valueField="id"
                defaultSelectedFirstOption
                value={3}
            />
        );

        expect(wrapper.props().value).toBe(3);
    });

    it('call onChange function for default option', () => {
        let changedValue;
        mount(
            <FoxSelect
                entities={entities}
                textField="name"
                valueField="id"
                defaultSelectedFirstOption
                value={3}
                onChange={(value: any) => (changedValue = value)}
            />
        );

        expect(changedValue).toBe(3);
    });
});
