import * as React from 'react';
import {storiesOf} from '@storybook/react';
import {FoxTable} from './FoxTable';
import 'antd/dist/antd.less';
import {FoxActionType} from '../manageActions/FoxActionType';
import {action} from '@storybook/addon-actions';

const values = [
    {
        name: 'element 1',
        description: 'description 1',
    },
    {
        name: 'element 2',
        description: 'description 2',
    },
    {
        name: 'element 3',
        description: 'description 3',
    },
    {
        name: 'element 4',
        description: 'description 4',
    },
    {
        name: 'element 5',
        description: 'description 5',
    },
];

storiesOf('Table', module)
    .add('simple', () => (
        <FoxTable
            tableData={values}
            tableColumns={[
                {
                    title: 'Name',
                    dataIndex: 'name',
                    key: 'name',
                },
                {
                    title: 'Description',
                    dataIndex: 'description',
                    key: 'description',
                },
            ]}
            onAction={() => ''}
        />
    ))
    .add('with actions', () => (
        <FoxTable
            tableData={values}
            tableColumns={[
                {
                    title: 'Name',
                    dataIndex: 'name',
                    key: 'name',
                },
                {
                    title: 'Description',
                    dataIndex: 'description',
                    key: 'description',
                },
            ]}
            actions={[
                {
                    name: 'Action 1',
                    type: FoxActionType.create,
                    icon: 'eye',
                },
                {
                    name: 'Action 2',
                    type: FoxActionType.update,
                    icon: 'edit',
                    action: () => action('Action clicked!'),
                },
            ]}
            onAction={() => action('Action clicked!')}
        />
    ))
    .add('expand rows', () => (
        <FoxTable
            tableData={values}
            tableColumns={[
                {
                    title: 'Name',
                    dataIndex: 'name',
                    key: 'name',
                },
                {
                    title: 'Description',
                    dataIndex: 'description',
                    key: 'description',
                },
            ]}
            actions={[
                {
                    name: 'Action 1',
                    type: FoxActionType.create,
                    icon: 'eye',
                },
                {
                    name: 'Action 2',
                    type: FoxActionType.update,
                    icon: 'edit',
                    action: () => action('Action clicked!'),
                },
            ]}
            onAction={() => action('Action clicked!')}
            expandedRowRender={() => 'this is great'}
        />
    ));
