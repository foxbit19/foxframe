import React, {useEffect, useState} from 'react';
import {Table, Divider, Icon, Input, Button} from 'antd';
import {ColumnProps, FilterDropdownProps} from 'antd/lib/table';
import FoxAction from '../manageActions/FoxAction';
import {FoxActionType} from '../manageActions/FoxActionType';
import Utility from '../utility';
import moment from 'moment';

interface FoxTableProps {
    tableColumns: Array<ColumnProps<any>>;
    tableData: any[];
    actions?: FoxAction[];
    onAction: (selectedActionType: FoxActionType, selectedEntity: any) => void;
    /**
     * Set a function to produce a ReactNode to render when a row is expanded
     */
    expandedRowRender?: (
        record: any,
        index: number,
        indent: number,
        expanded: boolean
    ) => React.ReactNode;
    showHeader?: boolean;
}

export const FoxTable: React.FC<FoxTableProps> = (props: FoxTableProps) => {
    const [columns, setColumns] = useState<Array<ColumnProps<any>>>([]);
    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');
    let searchInput: Input | null;

    const getFilterDropdown = (dataIndex: string, props: FilterDropdownProps) => (
        <div style={{padding: 8}}>
            <Input
                ref={node => (searchInput = node)}
                value={props.selectedKeys ? props.selectedKeys[0] : ''}
                onChange={e =>
                    props.setSelectedKeys
                        ? props.setSelectedKeys(e.target.value ? [e.target.value] : [])
                        : null
                }
                onPressEnter={() => {
                    if (props.selectedKeys && props.confirm) {
                        handleSearch(props.selectedKeys, props.confirm, dataIndex);
                    }
                }}
                style={{width: 188, marginBottom: 8, display: 'block'}}
            />
            <Button
                type="primary"
                onClick={() => {
                    if (props.selectedKeys && props.confirm) {
                        handleSearch(props.selectedKeys, props.confirm, dataIndex);
                    }
                }}
                icon="search"
                size="small"
                style={{width: 90, marginRight: 8}}
            >
                Cerca
            </Button>
            <Button
                onClick={() => {
                    if (props.clearFilters) {
                        handleReset(props.clearFilters);
                    }
                }}
                size="small"
                style={{width: 90}}
            >
                Reset
            </Button>
        </div>
    );

    const getFilterIcon = (filtered: boolean) => (
        <Icon type="search" style={{color: filtered ? '#1890ff' : undefined}} />
    );

    const getOnFilter = (dataIndex: string, value: any, record: any) =>
        record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase());

    const getOnFilterDropdownVisibleChange = (visible: boolean) => {
        if (visible) {
            setTimeout(() => {
                if (searchInput) {
                    searchInput.select();
                }
            });
        }
    };

    const getSorter = (dataIndex: string, a: any, b: any): number => {
        if (typeof a[dataIndex] === 'number') {
            return a[dataIndex] > b[dataIndex] ? 1 : -1;
        } else if (typeof a[dataIndex] === 'string') {
            return [a[dataIndex], b[dataIndex]].sort()[0] === a[dataIndex] ? 1 : -1;
        } else if (typeof a[dataIndex] === 'boolean') {
            return a[dataIndex] ? 1 : -1;
        } else if (typeof a[dataIndex] === 'object') {
            if (a[dataIndex] instanceof Date) {
                return [
                    moment(a[dataIndex]).format('YYYYMMDDHHmm'),
                    moment(b[dataIndex]).format('YYYYMMDDHHmm'),
                ].sort()[0] === moment(a[dataIndex]).format('YYYYMMDDHHmm')
                    ? 1
                    : -1;
            }
        }

        return 1;
    };

    /* const getCustomRender = (dataIndex:string, text: any, record: any, index: number) => searchedColumn === dataIndex ? <Highlighter
            highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
            searchWords={[searchText]}
            autoEscape
            textToHighlight={text.toString()}
          /> : text */

    const handleSearch = (selectedKeys: React.Key[], confirm: () => void, dataIndex: string) => {
        confirm();
        setSearchText(selectedKeys[0].toString());
        setSearchedColumn(dataIndex);
    };

    const handleReset = (clearFilters: () => void) => {
        clearFilters();
        setSearchText('');
    };

    const setSearchableColumns = (columns: ColumnProps<any>[]) => {
        for (const column of columns) {
            column.filterDropdown = (props: FilterDropdownProps) =>
                getFilterDropdown(column.dataIndex ? column.dataIndex : '', props);
            column.filterIcon = getFilterIcon;
            column.onFilter = (value: any, record: any) =>
                getOnFilter(column.dataIndex ? column.dataIndex : '', value, record);
            column.onFilterDropdownVisibleChange = getOnFilterDropdownVisibleChange;
            column.sorter = (a: any, b: any) =>
                getSorter(column.dataIndex ? column.dataIndex : '', a, b);
            column.sortDirections = ['descend', 'ascend'];
        }
    };

    useEffect(() => {
        const handleAction = (selectedAction: FoxAction, selectedEntity: any) => {
            if (selectedAction.type === FoxActionType.custom && selectedAction.action) {
                selectedAction.action(selectedEntity);
            } else {
                props.onAction(selectedAction.type, selectedEntity);
            }
        };

        // add search to each non-action column
        setSearchableColumns(props.tableColumns);

        if (
            props.actions &&
            !columns.find((column: ColumnProps<any>) => column.dataIndex === 'actions')
        ) {
            // add actions column
            setColumns([
                ...props.tableColumns,
                {
                    title: 'Azioni',
                    dataIndex: 'actions',
                    key: 'actions',
                    render: (text: string, entity: any) => (
                        <span>
                            {!props.actions
                                ? ''
                                : props.actions
                                      .filter((action: FoxAction) => {
                                          if (action.disabledField) {
                                              return !(
                                                  Utility.extractField(
                                                      entity,
                                                      action.disabledField
                                                  ) === action.disabledCheck
                                              );
                                          } else {
                                              return true;
                                          }
                                      })
                                      .map((action: FoxAction, index: number) => (
                                          <span key={index}>
                                              {index === 0 ? '' : <Divider type="vertical" />}
                                              <Icon
                                                  type={action.icon}
                                                  onClick={() => handleAction(action, entity)}
                                              />
                                          </span>
                                      ))}
                        </span>
                    ),
                },
            ]);
        } else {
            setColumns([...props.tableColumns]);
        }
        // eslint-disable-next-line
    }, []);

    return (
        <Table
            showHeader={props.showHeader}
            columns={columns}
            dataSource={props.tableData}
            pagination={{
                pageSize: props.tableData ? Math.floor((window.innerHeight - 210) / 55) : 0,
                position: 'bottom',
            }}
            tableLayout="fixed"
            expandedRowRender={props.expandedRowRender}
        />
    );
};
