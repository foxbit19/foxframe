import * as React from 'react';
import {storiesOf} from '@storybook/react';
import {FoxTabs} from './FoxTabs';
import {FoxLabel} from '../label/FoxLabel';
import 'antd/dist/antd.less';

const value = {
    name: 'element',
    description: 'description ',
    contact: 'contact field',
};

storiesOf('Tabs', module)
    .add('single tab form', () => (
        <FoxTabs
            config={[
                {
                    name: 'name',
                    label: 'Name label',
                    component: <FoxLabel />,
                    tabName: 'Info',
                },
                {
                    name: 'description',
                    label: 'Description label',
                    component: <FoxLabel />,
                    tabName: 'Info',
                },
            ]}
            entity={value}
        />
    ))
    .add('multi tab form', () => (
        <FoxTabs
            config={[
                {
                    name: 'name',
                    label: 'Name label',
                    component: <FoxLabel />,
                    tabName: 'Info',
                },
                {
                    name: 'description',
                    label: 'Description label',
                    component: <FoxLabel />,
                    tabName: 'Info',
                },
                {
                    name: 'contact',
                    label: 'Contact label',
                    component: <FoxLabel />,
                    tabName: 'Contact',
                },
            ]}
            entity={value}
        />
    ));
