import React, {useState, useEffect} from 'react';
import {Tabs} from 'antd';
import FormConfig from '../FormConfig';
import {FoxDynamicForm} from '../dynamicForm/DynamicForm';

const {TabPane} = Tabs;

interface Props {
    config: FormConfig[];
    entity?: any;
    wrappedComponentRef?: any;
    onFieldChange?: (changedFieldName: string) => void;
    /**
     * Force rendering each tab instead lazy loading
     */
    forceRender?: boolean;
}

export const FoxTabs: React.FC<Props> = (props: Props) => {
    const [tabNames, setTabNames] = useState<Array<string>>([]);

    useEffect(() => {
        const findTabNames = (): Array<string> => {
            if (props.config) {
                let names: string[] = [];
                for (const config of props.config) {
                    if (config.tabName) {
                        names.push(config.tabName);
                    }
                }
                return names.filter(distinct);
            } else {
                return [];
            }
        };
        const distinct = (value: any, index: number, self: any) => self.indexOf(value) === index;

        setTabNames(findTabNames());
    }, [props.config]);

    return (
        <div>
            {tabNames && tabNames.length > 0 ? (
                <Tabs defaultActiveKey="0">
                    {tabNames.map((tabName: string, index: number) => {
                        if (tabName) {
                            return (
                                <TabPane
                                    tab={tabName}
                                    key={`${index}`}
                                    forceRender={props.forceRender}
                                >
                                    <FoxDynamicForm
                                        config={props.config.filter(
                                            (config: FormConfig) => config.tabName === tabName
                                        )}
                                        entity={props.entity}
                                        wrappedComponentRef={props.wrappedComponentRef}
                                        onFieldChange={
                                            props.onFieldChange ? props.onFieldChange : () => ''
                                        }
                                    />
                                </TabPane>
                            );
                        } else {
                            return null;
                        }
                    })}
                </Tabs>
            ) : (
                <FoxDynamicForm
                    config={props.config}
                    entity={props.entity}
                    wrappedComponentRef={props.wrappedComponentRef}
                    onFieldChange={props.onFieldChange ? props.onFieldChange : () => ''}
                />
            )}
        </div>
    );
};
