import * as React from 'react';
import {storiesOf} from '@storybook/react';
import {FoxTimeline} from './FoxTimeline';
import 'antd/dist/antd.less';

const values = [
    {
        name: 'element 1',
        description: 'description 1',
    },
    {
        name: 'element 2',
        description: 'description 2',
    },
    {
        name: 'element 3',
        description: 'description 3',
    },
    {
        name: 'element 4',
        description: 'description 4',
    },
    {
        name: 'element 5',
        description: 'description 5',
    },
];

storiesOf('Timeline', module)
    .add('simple', () => (
        <FoxTimeline
            value={values}
            fields={[
                {
                    name: 'name',
                },
            ]}
        />
    ))
    .add('custom format', () => (
        <FoxTimeline
            value={values}
            fields={[
                {
                    name: 'name',
                    format: (value: string) => <span style={{fontWeight: 'bold'}}>{value}</span>,
                },
            ]}
        />
    ))
    .add('more fields', () => (
        <FoxTimeline
            value={values}
            fields={[
                {
                    name: 'name',
                },
                {
                    name: 'description',
                },
            ]}
        />
    ));
