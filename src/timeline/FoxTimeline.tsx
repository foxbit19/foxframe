import React from 'react';
import {Timeline, Icon} from 'antd';

interface FieldFormat {
    name: string;
    format?: (value: any) => void;
}
interface Props {
    value?: any[];
    /**
     *  fields to show with the format function
     */
    fields: FieldFormat[];
}

export const FoxTimeline: React.FC<Props> = (props: Props) => {
    return (
        <Timeline>
            {props.value
                ? props.value.map((entity: any, index: number) => (
                      <Timeline.Item
                          key={index}
                          dot={
                              props.value && index === props.value.length - 1 ? (
                                  <Icon type="up-circle" />
                              ) : (
                                  ''
                              )
                          }
                      >
                          {props.fields.map((field: FieldFormat) =>
                              field.format ? field.format(entity[field.name]) : entity[field.name]
                          )}
                      </Timeline.Item>
                  ))
                : 'no value specified'}
        </Timeline>
    );
};
