export default class Utility {
    public static extractField(entity: any, fieldName: string) {
        const splitFields = fieldName.split('.');
        let field: any = entity;

        for (const splitField of splitFields) {
            field = field[splitField];
        }
        return field;
    }
}
